#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# create a new 'OpenFOAMReader'
afoam = OpenFOAMReader(registrationName='a.foam', FileName='./3-simulations/a.foam')
afoam.MeshRegions = ['internalMesh']
afoam.CellArrays = ['U', 'alpha.water']

# get animation scene
animationScene1 = GetAnimationScene()

# get the time-keeper
timeKeeper1 = GetTimeKeeper()

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')

# get the material library
materialLibrary1 = GetMaterialLibrary()

# get display properties
afoamDisplay = GetDisplayProperties(afoam, view=renderView1)

# update animation scene based on data timesteps
animationScene1.UpdateAnimationUsingDataTimeSteps()

animationScene1.GoToLast()

renderView1.ResetActiveCameraToPositiveY()

# reset view to fit data
renderView1.ResetCamera(False)

# create a new 'Slice'
slice1 = Slice(registrationName='Slice1', Input=afoam)
slice1.SliceType = 'Plane'
slice1.HyperTreeGridSlicer = 'Plane'
slice1.SliceOffsetValues = [0.0]

# init the 'Plane' selected for 'SliceType'
slice1.SliceType.Origin = [1.0, 0.0, 0.0]

# init the 'Plane' selected for 'HyperTreeGridSlicer'
slice1.HyperTreeGridSlicer.Origin = [1.0, 0.0, 0.0]

# show data in view
slice1Display = Show(slice1, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
slice1Display.Representation = 'Surface'
slice1Display.ColorArrayName = [None, '']
slice1Display.SelectTCoordArray = 'None'
slice1Display.SelectNormalArray = 'None'
slice1Display.SelectTangentArray = 'None'
slice1Display.OSPRayScaleArray = 'U'
slice1Display.OSPRayScaleFunction = 'PiecewiseFunction'
slice1Display.SelectOrientationVectors = 'U'
slice1Display.ScaleFactor = 0.12000000476837158
slice1Display.SelectScaleArray = 'None'
slice1Display.GlyphType = 'Arrow'
slice1Display.GlyphTableIndexArray = 'None'
slice1Display.GaussianRadius = 0.006000000238418579
slice1Display.SetScaleArray = ['POINTS', 'U']
slice1Display.ScaleTransferFunction = 'PiecewiseFunction'
slice1Display.OpacityArray = ['POINTS', 'U']
slice1Display.OpacityTransferFunction = 'PiecewiseFunction'
slice1Display.DataAxesGrid = 'GridAxesRepresentation'
slice1Display.PolarAxes = 'PolarAxesRepresentation'
slice1Display.SelectInputVectors = ['POINTS', 'U']
slice1Display.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
slice1Display.ScaleTransferFunction.Points = [-1.6811262369155884, 0.0, 0.5, 0.0, 0.6698133945465088, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
slice1Display.OpacityTransferFunction.Points = [-1.6811262369155884, 0.0, 0.5, 0.0, 0.6698133945465088, 1.0, 0.5, 0.0]

# hide data in view
Hide(afoam, renderView1)

# Properties modified on slice1.SliceType
slice1.SliceType.Normal = [0.0, 1.0, 0.0]

# update the view to ensure updated data information
renderView1.Update()

# toggle interactive widget visibility (only when running from the GUI)
HideInteractiveWidgets(proxy=slice1.SliceType)

# create a new 'Threshold'
threshold1 = Threshold(registrationName='Threshold1', Input=slice1)
threshold1.Scalars = ['POINTS', 'alpha.water']
threshold1.LowerThreshold = -1.0056376605434367e-21
threshold1.UpperThreshold = 1.0000061988830566

# show data in view
threshold1Display = Show(threshold1, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
threshold1Display.Representation = 'Surface'
threshold1Display.ColorArrayName = [None, '']
threshold1Display.SelectTCoordArray = 'None'
threshold1Display.SelectNormalArray = 'None'
threshold1Display.SelectTangentArray = 'None'
threshold1Display.OSPRayScaleArray = 'U'
threshold1Display.OSPRayScaleFunction = 'PiecewiseFunction'
threshold1Display.SelectOrientationVectors = 'U'
threshold1Display.ScaleFactor = 0.8
threshold1Display.SelectScaleArray = 'None'
threshold1Display.GlyphType = 'Arrow'
threshold1Display.GlyphTableIndexArray = 'None'
threshold1Display.GaussianRadius = 0.04
threshold1Display.SetScaleArray = ['POINTS', 'U']
threshold1Display.ScaleTransferFunction = 'PiecewiseFunction'
threshold1Display.OpacityArray = ['POINTS', 'U']
threshold1Display.OpacityTransferFunction = 'PiecewiseFunction'
threshold1Display.DataAxesGrid = 'GridAxesRepresentation'
threshold1Display.PolarAxes = 'PolarAxesRepresentation'
threshold1Display.ScalarOpacityUnitDistance = 0.3831259342910861
threshold1Display.OpacityArrayName = ['POINTS', 'U']
threshold1Display.SelectInputVectors = ['POINTS', 'U']
threshold1Display.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
threshold1Display.ScaleTransferFunction.Points = [-3.305668830871582, 0.0, 0.5, 0.0, 1.356733798980713, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
threshold1Display.OpacityTransferFunction.Points = [-3.305668830871582, 0.0, 0.5, 0.0, 1.356733798980713, 1.0, 0.5, 0.0]

# hide data in view
Hide(slice1, renderView1)

# Properties modified on threshold1
threshold1.LowerThreshold = -0.5

# Properties modified on threshold1
threshold1.UpperThreshold = 0.3

# Properties modified on threshold1
threshold1.LowerThreshold = 0.3

# Properties modified on threshold1
threshold1.UpperThreshold = 1.5

# update the view to ensure updated data information
renderView1.Update()

# set scalar coloring
ColorBy(threshold1Display, ('POINTS', 'U', 'Magnitude'))

# rescale color and/or opacity maps used to include current data range
threshold1Display.RescaleTransferFunctionToDataRange(True, False)

# show color bar/color legend
threshold1Display.SetScalarBarVisibility(renderView1, True)

# get color transfer function/color map for 'U'
uLUT = GetColorTransferFunction('U')

# get opacity transfer function/opacity map for 'U'
uPWF = GetOpacityTransferFunction('U')

# get 2D transfer function for 'U'
uTF2D = GetTransferFunction2D('U')

# Apply a preset using its name. Note this may not work as expected when presets have duplicate names.
uLUT.ApplyPreset('Viridis (matplotlib)', True)

# Properties modified on uLUT
uLUT.NumberOfTableValues = 18

# get color legend/bar for uLUT in view renderView1
uLUTColorBar = GetScalarBar(uLUT, renderView1)

# Properties modified on uLUTColorBar
uLUTColorBar.Title = 'v, m/s'
uLUTColorBar.ComponentTitle = ''
uLUTColorBar.RangeLabelFormat = '%-#6.2g'

# change scalar bar placement
uLUTColorBar.Orientation = 'Horizontal'
uLUTColorBar.WindowLocation = 'Any Location'
uLUTColorBar.Position = [0.6459658580413298, 0.8919143576826196]
uLUTColorBar.ScalarBarLength = 0.3299999999999994

# set active source
SetActiveSource(slice1)

# show data in view
slice1Display = Show(slice1, renderView1, 'GeometryRepresentation')

# update the view to ensure updated data information
renderView1.Update()

# set active source
SetActiveSource(threshold1)

# create a new 'Surface Vectors'
surfaceVectors1 = SurfaceVectors(registrationName='SurfaceVectors1', Input=threshold1)
surfaceVectors1.SelectInputVectors = ['POINTS', 'U']

# show data in view
surfaceVectors1Display = Show(surfaceVectors1, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
surfaceVectors1Display.Representation = 'Surface'
surfaceVectors1Display.ColorArrayName = ['POINTS', 'U']
surfaceVectors1Display.LookupTable = uLUT
surfaceVectors1Display.SelectTCoordArray = 'None'
surfaceVectors1Display.SelectNormalArray = 'None'
surfaceVectors1Display.SelectTangentArray = 'None'
surfaceVectors1Display.OSPRayScaleArray = 'U'
surfaceVectors1Display.OSPRayScaleFunction = 'PiecewiseFunction'
surfaceVectors1Display.SelectOrientationVectors = 'U'
surfaceVectors1Display.ScaleFactor = 0.8
surfaceVectors1Display.SelectScaleArray = 'None'
surfaceVectors1Display.GlyphType = 'Arrow'
surfaceVectors1Display.GlyphTableIndexArray = 'None'
surfaceVectors1Display.GaussianRadius = 0.04
surfaceVectors1Display.SetScaleArray = ['POINTS', 'U']
surfaceVectors1Display.ScaleTransferFunction = 'PiecewiseFunction'
surfaceVectors1Display.OpacityArray = ['POINTS', 'U']
surfaceVectors1Display.OpacityTransferFunction = 'PiecewiseFunction'
surfaceVectors1Display.DataAxesGrid = 'GridAxesRepresentation'
surfaceVectors1Display.PolarAxes = 'PolarAxesRepresentation'
surfaceVectors1Display.ScalarOpacityFunction = uPWF
surfaceVectors1Display.ScalarOpacityUnitDistance = 0.5886030884997315
surfaceVectors1Display.OpacityArrayName = ['POINTS', 'U']
surfaceVectors1Display.SelectInputVectors = ['POINTS', 'U']
surfaceVectors1Display.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
surfaceVectors1Display.ScaleTransferFunction.Points = [-3.305668830871582, 0.0, 0.5, 0.0, 0.8216521143913269, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
surfaceVectors1Display.OpacityTransferFunction.Points = [-3.305668830871582, 0.0, 0.5, 0.0, 0.8216521143913269, 1.0, 0.5, 0.0]

# hide data in view
Hide(threshold1, renderView1)

# show color bar/color legend
surfaceVectors1Display.SetScalarBarVisibility(renderView1, True)

# update the view to ensure updated data information
renderView1.Update()

# set active source
SetActiveSource(threshold1)

# show data in view
threshold1Display = Show(threshold1, renderView1, 'UnstructuredGridRepresentation')

# show color bar/color legend
threshold1Display.SetScalarBarVisibility(renderView1, True)

# update the view to ensure updated data information
renderView1.Update()

# set active source
SetActiveSource(surfaceVectors1)

# create a new 'Glyph'
glyph1 = Glyph(registrationName='Glyph1', Input=surfaceVectors1,
    GlyphType='Arrow')
glyph1.OrientationArray = ['POINTS', 'U']
glyph1.ScaleArray = ['POINTS', 'No scale array']
glyph1.ScaleFactor = 0.8
glyph1.GlyphTransform = 'Transform2'

# show data in view
glyph1Display = Show(glyph1, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
glyph1Display.Representation = 'Surface'
glyph1Display.ColorArrayName = ['POINTS', 'U']
glyph1Display.LookupTable = uLUT
glyph1Display.SelectTCoordArray = 'None'
glyph1Display.SelectNormalArray = 'None'
glyph1Display.SelectTangentArray = 'None'
glyph1Display.OSPRayScaleArray = 'U'
glyph1Display.OSPRayScaleFunction = 'PiecewiseFunction'
glyph1Display.SelectOrientationVectors = 'U'
glyph1Display.ScaleFactor = 0.9451546907424927
glyph1Display.SelectScaleArray = 'None'
glyph1Display.GlyphType = 'Arrow'
glyph1Display.GlyphTableIndexArray = 'None'
glyph1Display.GaussianRadius = 0.04725773453712463
glyph1Display.SetScaleArray = ['POINTS', 'U']
glyph1Display.ScaleTransferFunction = 'PiecewiseFunction'
glyph1Display.OpacityArray = ['POINTS', 'U']
glyph1Display.OpacityTransferFunction = 'PiecewiseFunction'
glyph1Display.DataAxesGrid = 'GridAxesRepresentation'
glyph1Display.PolarAxes = 'PolarAxesRepresentation'
glyph1Display.SelectInputVectors = ['POINTS', 'U']
glyph1Display.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
glyph1Display.ScaleTransferFunction.Points = [-3.305668830871582, 0.0, 0.5, 0.0, 0.8216521143913269, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
glyph1Display.OpacityTransferFunction.Points = [-3.305668830871582, 0.0, 0.5, 0.0, 0.8216521143913269, 1.0, 0.5, 0.0]

# show color bar/color legend
glyph1Display.SetScalarBarVisibility(renderView1, True)

# update the view to ensure updated data information
renderView1.Update()

# Properties modified on glyph1
glyph1.ScaleArray = ['POINTS', 'U']

# update the view to ensure updated data information
renderView1.Update()

# Properties modified on glyph1
glyph1.ScaleFactor = 0.9736480756124305

# update the view to ensure updated data information
renderView1.Update()

# turn off scalar coloring
ColorBy(glyph1Display, None)

# Hide the scalar bar for this color map if no visible data is colored by it.
HideScalarBarIfNotNeeded(uLUT, renderView1)

# change solid color
glyph1Display.DiffuseColor = [0.0, 0.0, 0.0]

# Properties modified on glyph1
glyph1.ScaleFactor = 0.1

# update the view to ensure updated data information
renderView1.Update()

# Properties modified on glyph1
glyph1.MaximumNumberOfSamplePoints = 500

# Properties modified on glyph1
glyph1.ScaleFactor = 0.2

# Properties modified on glyph1
glyph1.ScaleFactor = 0.1

# Properties modified on glyph1
glyph1.ScaleFactor = 0.05

# Properties modified on glyph1
glyph1.MaximumNumberOfSamplePoints = 1000

# create a new 'Text'
text1 = Text(registrationName='Text1')

# show data in view
text1Display = Show(text1, renderView1, 'TextSourceRepresentation')

# Properties modified on text1
text1.Text = 'Without obstacle'

# update the view to ensure updated data information
renderView1.Update()

# Properties modified on text1Display
text1Display.FontFamily = 'Courier'

# create a new 'OpenFOAMReader'
afoam_1 = OpenFOAMReader(registrationName='a.foam', FileName='./4-simulations-with-sHM-obstacle/a.foam')
afoam_1.MeshRegions = ['internalMesh']
afoam_1.CellArrays = ['U', 'alpha.water']

# show data in view
afoam_1Display = Show(afoam_1, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
afoam_1Display.Representation = 'Surface'
afoam_1Display.ColorArrayName = [None, '']
afoam_1Display.SelectTCoordArray = 'None'
afoam_1Display.SelectNormalArray = 'None'
afoam_1Display.SelectTangentArray = 'None'
afoam_1Display.OSPRayScaleArray = 'U'
afoam_1Display.OSPRayScaleFunction = 'PiecewiseFunction'
afoam_1Display.SelectOrientationVectors = 'U'
afoam_1Display.SelectScaleArray = 'None'
afoam_1Display.GlyphType = 'Arrow'
afoam_1Display.GlyphTableIndexArray = 'None'
afoam_1Display.GaussianRadius = 0.05
afoam_1Display.SetScaleArray = ['POINTS', 'U']
afoam_1Display.ScaleTransferFunction = 'PiecewiseFunction'
afoam_1Display.OpacityArray = ['POINTS', 'U']
afoam_1Display.OpacityTransferFunction = 'PiecewiseFunction'
afoam_1Display.DataAxesGrid = 'GridAxesRepresentation'
afoam_1Display.PolarAxes = 'PolarAxesRepresentation'
afoam_1Display.ScalarOpacityUnitDistance = 0.17601572776675842
afoam_1Display.OpacityArrayName = ['POINTS', 'U']
afoam_1Display.SelectInputVectors = ['POINTS', 'U']
afoam_1Display.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
afoam_1Display.ScaleTransferFunction.Points = [-2.5653088092803955, 0.0, 0.5, 0.0, 1.1079399585723877, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
afoam_1Display.OpacityTransferFunction.Points = [-2.5653088092803955, 0.0, 0.5, 0.0, 1.1079399585723877, 1.0, 0.5, 0.0]

# update the view to ensure updated data information
renderView1.Update()

# create a new 'Transform'
transform1 = Transform(registrationName='Transform1', Input=afoam_1)
transform1.Transform = 'Transform'

# show data in view
transform1Display = Show(transform1, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
transform1Display.Representation = 'Surface'
transform1Display.ColorArrayName = [None, '']
transform1Display.SelectTCoordArray = 'None'
transform1Display.SelectNormalArray = 'None'
transform1Display.SelectTangentArray = 'None'
transform1Display.OSPRayScaleArray = 'U'
transform1Display.OSPRayScaleFunction = 'PiecewiseFunction'
transform1Display.SelectOrientationVectors = 'U'
transform1Display.SelectScaleArray = 'None'
transform1Display.GlyphType = 'Arrow'
transform1Display.GlyphTableIndexArray = 'None'
transform1Display.GaussianRadius = 0.05
transform1Display.SetScaleArray = ['POINTS', 'U']
transform1Display.ScaleTransferFunction = 'PiecewiseFunction'
transform1Display.OpacityArray = ['POINTS', 'U']
transform1Display.OpacityTransferFunction = 'PiecewiseFunction'
transform1Display.DataAxesGrid = 'GridAxesRepresentation'
transform1Display.PolarAxes = 'PolarAxesRepresentation'
transform1Display.ScalarOpacityUnitDistance = 0.17601572776675842
transform1Display.OpacityArrayName = ['POINTS', 'U']
transform1Display.SelectInputVectors = ['POINTS', 'U']
transform1Display.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
transform1Display.ScaleTransferFunction.Points = [-2.5653088092803955, 0.0, 0.5, 0.0, 1.1079399585723877, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
transform1Display.OpacityTransferFunction.Points = [-2.5653088092803955, 0.0, 0.5, 0.0, 1.1079399585723877, 1.0, 0.5, 0.0]

# hide data in view
Hide(afoam_1, renderView1)

# Properties modified on transform1.Transform
transform1.Transform.Translate = [0.0, 0.0, -2.3]

# update the view to ensure updated data information
renderView1.Update()

# create a new 'Slice'
slice2 = Slice(registrationName='Slice2', Input=transform1)
slice2.SliceType = 'Plane'
slice2.HyperTreeGridSlicer = 'Plane'
slice2.SliceOffsetValues = [0.0]

# init the 'Plane' selected for 'SliceType'
slice2.SliceType.Origin = [0.0, 0.0, -2.299999952316284]

# init the 'Plane' selected for 'HyperTreeGridSlicer'
slice2.HyperTreeGridSlicer.Origin = [0.0, 0.0, -2.299999952316284]

# show data in view
slice2Display = Show(slice2, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
slice2Display.Representation = 'Surface'
slice2Display.ColorArrayName = [None, '']
slice2Display.SelectTCoordArray = 'None'
slice2Display.SelectNormalArray = 'None'
slice2Display.SelectTangentArray = 'None'
slice2Display.OSPRayScaleArray = 'U'
slice2Display.OSPRayScaleFunction = 'PiecewiseFunction'
slice2Display.SelectOrientationVectors = 'U'
slice2Display.ScaleFactor = 0.30000000000000004
slice2Display.SelectScaleArray = 'None'
slice2Display.GlyphType = 'Arrow'
slice2Display.GlyphTableIndexArray = 'None'
slice2Display.GaussianRadius = 0.015
slice2Display.SetScaleArray = ['POINTS', 'U']
slice2Display.ScaleTransferFunction = 'PiecewiseFunction'
slice2Display.OpacityArray = ['POINTS', 'U']
slice2Display.OpacityTransferFunction = 'PiecewiseFunction'
slice2Display.DataAxesGrid = 'GridAxesRepresentation'
slice2Display.PolarAxes = 'PolarAxesRepresentation'
slice2Display.SelectInputVectors = ['POINTS', 'U']
slice2Display.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
slice2Display.ScaleTransferFunction.Points = [-2.2700257301330566, 0.0, 0.5, 0.0, 0.5298245549201965, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
slice2Display.OpacityTransferFunction.Points = [-2.2700257301330566, 0.0, 0.5, 0.0, 0.5298245549201965, 1.0, 0.5, 0.0]

# hide data in view
Hide(transform1, renderView1)

# toggle interactive widget visibility (only when running from the GUI)
HideInteractiveWidgets(proxy=slice2.SliceType)

# Properties modified on slice2.SliceType
slice2.SliceType.Normal = [0.0, 1.0, 0.0]

# update the view to ensure updated data information
renderView1.Update()

# Hide orientation axes
renderView1.OrientationAxesVisibility = 0

# create a new 'Threshold'
threshold2 = Threshold(registrationName='Threshold2', Input=slice2)
threshold2.Scalars = ['POINTS', 'alpha.water']
threshold2.LowerThreshold = -3.0035478630452417e-05
threshold2.UpperThreshold = 1.000086784362793

# show data in view
threshold2Display = Show(threshold2, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
threshold2Display.Representation = 'Surface'
threshold2Display.ColorArrayName = [None, '']
threshold2Display.SelectTCoordArray = 'None'
threshold2Display.SelectNormalArray = 'None'
threshold2Display.SelectTangentArray = 'None'
threshold2Display.OSPRayScaleArray = 'U'
threshold2Display.OSPRayScaleFunction = 'PiecewiseFunction'
threshold2Display.SelectOrientationVectors = 'U'
threshold2Display.SelectScaleArray = 'None'
threshold2Display.GlyphType = 'Arrow'
threshold2Display.GlyphTableIndexArray = 'None'
threshold2Display.GaussianRadius = 0.05
threshold2Display.SetScaleArray = ['POINTS', 'U']
threshold2Display.ScaleTransferFunction = 'PiecewiseFunction'
threshold2Display.OpacityArray = ['POINTS', 'U']
threshold2Display.OpacityTransferFunction = 'PiecewiseFunction'
threshold2Display.DataAxesGrid = 'GridAxesRepresentation'
threshold2Display.PolarAxes = 'PolarAxesRepresentation'
threshold2Display.ScalarOpacityUnitDistance = 0.4548978716440952
threshold2Display.OpacityArrayName = ['POINTS', 'U']
threshold2Display.SelectInputVectors = ['POINTS', 'U']
threshold2Display.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
threshold2Display.ScaleTransferFunction.Points = [-2.558152437210083, 0.0, 0.5, 0.0, 0.6748281121253967, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
threshold2Display.OpacityTransferFunction.Points = [-2.558152437210083, 0.0, 0.5, 0.0, 0.6748281121253967, 1.0, 0.5, 0.0]

# hide data in view
Hide(slice2, renderView1)

# Properties modified on threshold2
threshold2.LowerThreshold = 0.3

# Properties modified on threshold2
threshold2.UpperThreshold = 1.5

# update the view to ensure updated data information
renderView1.Update()

# set scalar coloring
ColorBy(threshold2Display, ('POINTS', 'U', 'Magnitude'))

# rescale color and/or opacity maps used to include current data range
threshold2Display.RescaleTransferFunctionToDataRange(True, False)

# show color bar/color legend
threshold2Display.SetScalarBarVisibility(renderView1, True)

# create a new 'Surface Flow'
surfaceFlow1 = SurfaceFlow(registrationName='SurfaceFlow1', Input=threshold2)
surfaceFlow1.SelectInputVectors = ['POINTS', 'U']

# show data in view
surfaceFlow1Display = Show(surfaceFlow1, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
surfaceFlow1Display.Representation = 'Surface'
surfaceFlow1Display.ColorArrayName = ['POINTS', 'U']
surfaceFlow1Display.LookupTable = uLUT
surfaceFlow1Display.SelectTCoordArray = 'None'
surfaceFlow1Display.SelectNormalArray = 'None'
surfaceFlow1Display.SelectTangentArray = 'None'
surfaceFlow1Display.OSPRayScaleArray = 'Surface Flow'
surfaceFlow1Display.OSPRayScaleFunction = 'PiecewiseFunction'
surfaceFlow1Display.SelectOrientationVectors = 'None'
surfaceFlow1Display.ScaleFactor = 0.1
surfaceFlow1Display.SelectScaleArray = 'None'
surfaceFlow1Display.GlyphType = 'Arrow'
surfaceFlow1Display.GlyphTableIndexArray = 'None'
surfaceFlow1Display.GaussianRadius = 0.005
surfaceFlow1Display.SetScaleArray = ['POINTS', 'Surface Flow']
surfaceFlow1Display.ScaleTransferFunction = 'PiecewiseFunction'
surfaceFlow1Display.OpacityArray = ['POINTS', 'Surface Flow']
surfaceFlow1Display.OpacityTransferFunction = 'PiecewiseFunction'
surfaceFlow1Display.DataAxesGrid = 'GridAxesRepresentation'
surfaceFlow1Display.PolarAxes = 'PolarAxesRepresentation'
surfaceFlow1Display.ScalarOpacityFunction = uPWF
surfaceFlow1Display.ScalarOpacityUnitDistance = 0.0
surfaceFlow1Display.OpacityArrayName = ['POINTS', 'Surface Flow']
surfaceFlow1Display.SelectInputVectors = ['POINTS', 'U']
surfaceFlow1Display.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
surfaceFlow1Display.ScaleTransferFunction.Points = [6.159986150798407e-05, 0.0, 0.5, 0.0, 6.161475903354585e-05, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
surfaceFlow1Display.OpacityTransferFunction.Points = [6.159986150798407e-05, 0.0, 0.5, 0.0, 6.161475903354585e-05, 1.0, 0.5, 0.0]

# hide data in view
Hide(threshold2, renderView1)

# show color bar/color legend
surfaceFlow1Display.SetScalarBarVisibility(renderView1, True)

# set active source
SetActiveSource(threshold2)

# set active source
SetActiveSource(surfaceFlow1)

# set active source
SetActiveSource(threshold2)

# hide data in view
Hide(surfaceFlow1, renderView1)

# show data in view
threshold2Display = Show(threshold2, renderView1, 'UnstructuredGridRepresentation')

# show color bar/color legend
threshold2Display.SetScalarBarVisibility(renderView1, True)

# destroy surfaceFlow1
Delete(surfaceFlow1)
del surfaceFlow1

# update the view to ensure updated data information
renderView1.Update()

# create a new 'Surface Vectors'
surfaceVectors2 = SurfaceVectors(registrationName='SurfaceVectors2', Input=threshold2)
surfaceVectors2.SelectInputVectors = ['POINTS', 'U']

# show data in view
surfaceVectors2Display = Show(surfaceVectors2, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
surfaceVectors2Display.Representation = 'Surface'
surfaceVectors2Display.ColorArrayName = ['POINTS', 'U']
surfaceVectors2Display.LookupTable = uLUT
surfaceVectors2Display.SelectTCoordArray = 'None'
surfaceVectors2Display.SelectNormalArray = 'None'
surfaceVectors2Display.SelectTangentArray = 'None'
surfaceVectors2Display.OSPRayScaleArray = 'U'
surfaceVectors2Display.OSPRayScaleFunction = 'PiecewiseFunction'
surfaceVectors2Display.SelectOrientationVectors = 'U'
surfaceVectors2Display.SelectScaleArray = 'None'
surfaceVectors2Display.GlyphType = 'Arrow'
surfaceVectors2Display.GlyphTableIndexArray = 'None'
surfaceVectors2Display.GaussianRadius = 0.05
surfaceVectors2Display.SetScaleArray = ['POINTS', 'U']
surfaceVectors2Display.ScaleTransferFunction = 'PiecewiseFunction'
surfaceVectors2Display.OpacityArray = ['POINTS', 'U']
surfaceVectors2Display.OpacityTransferFunction = 'PiecewiseFunction'
surfaceVectors2Display.DataAxesGrid = 'GridAxesRepresentation'
surfaceVectors2Display.PolarAxes = 'PolarAxesRepresentation'
surfaceVectors2Display.ScalarOpacityFunction = uPWF
surfaceVectors2Display.ScalarOpacityUnitDistance = 0.6132707257465625
surfaceVectors2Display.OpacityArrayName = ['POINTS', 'U']
surfaceVectors2Display.SelectInputVectors = ['POINTS', 'U']
surfaceVectors2Display.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
surfaceVectors2Display.ScaleTransferFunction.Points = [-2.558152437210083, 0.0, 0.5, 0.0, 0.40792450308799744, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
surfaceVectors2Display.OpacityTransferFunction.Points = [-2.558152437210083, 0.0, 0.5, 0.0, 0.40792450308799744, 1.0, 0.5, 0.0]

# hide data in view
Hide(threshold2, renderView1)

# show color bar/color legend
surfaceVectors2Display.SetScalarBarVisibility(renderView1, True)

# set active source
SetActiveSource(threshold2)

# show data in view
threshold2Display = Show(threshold2, renderView1, 'UnstructuredGridRepresentation')

# show color bar/color legend
threshold2Display.SetScalarBarVisibility(renderView1, True)

# set active source
SetActiveSource(slice2)

# show data in view
slice2Display = Show(slice2, renderView1, 'GeometryRepresentation')

# update the view to ensure updated data information
renderView1.Update()

# set active source
SetActiveSource(surfaceVectors2)

# create a new 'Glyph'
glyph2 = Glyph(registrationName='Glyph2', Input=surfaceVectors2,
    GlyphType='Arrow')
glyph2.OrientationArray = ['POINTS', 'U']
glyph2.ScaleArray = ['POINTS', 'U']
glyph2.GlyphTransform = 'Transform2'
glyph2.ScaleFactor = 0.05

# show data in view
glyph2Display = Show(glyph2, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
glyph2Display.Representation = 'Surface'
glyph2Display.ColorArrayName = ['POINTS', 'U']
glyph2Display.LookupTable = uLUT
glyph2Display.SelectTCoordArray = 'None'
glyph2Display.SelectNormalArray = 'None'
glyph2Display.SelectTangentArray = 'None'
glyph2Display.OSPRayScaleArray = 'U'
glyph2Display.OSPRayScaleFunction = 'PiecewiseFunction'
glyph2Display.SelectOrientationVectors = 'U'
glyph2Display.ScaleFactor = 1.182781982421875
glyph2Display.SelectScaleArray = 'None'
glyph2Display.GlyphType = 'Arrow'
glyph2Display.GlyphTableIndexArray = 'None'
glyph2Display.GaussianRadius = 0.059139099121093754
glyph2Display.SetScaleArray = ['POINTS', 'U']
glyph2Display.ScaleTransferFunction = 'PiecewiseFunction'
glyph2Display.OpacityArray = ['POINTS', 'U']
glyph2Display.OpacityTransferFunction = 'PiecewiseFunction'
glyph2Display.DataAxesGrid = 'GridAxesRepresentation'
glyph2Display.PolarAxes = 'PolarAxesRepresentation'
glyph2Display.SelectInputVectors = ['POINTS', 'U']
glyph2Display.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
glyph2Display.ScaleTransferFunction.Points = [-2.558152437210083, 0.0, 0.5, 0.0, 0.40792450308799744, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
glyph2Display.OpacityTransferFunction.Points = [-2.558152437210083, 0.0, 0.5, 0.0, 0.40792450308799744, 1.0, 0.5, 0.0]

# show color bar/color legend
glyph2Display.SetScalarBarVisibility(renderView1, True)

# set active source
SetActiveSource(glyph1)

# set active source
SetActiveSource(glyph2)

# set active source
SetActiveSource(glyph1)

# set active source
SetActiveSource(glyph2)

# turn off scalar coloring
ColorBy(glyph2Display, None)

# Hide the scalar bar for this color map if no visible data is colored by it.
HideScalarBarIfNotNeeded(uLUT, renderView1)

# change solid color
glyph2Display.DiffuseColor = [0.0, 0.0, 0.0]

# create a new 'Text'
text2 = Text(registrationName='Text2')

# show data in view
text2Display = Show(text2, renderView1, 'TextSourceRepresentation')

# Properties modified on text2
text2.Text = 'With obstacle'

# update the view to ensure updated data information
renderView1.Update()

# Properties modified on text2Display
text2Display.WindowLocation = 'Lower Left Corner'

# Properties modified on text2Display
text2Display.FontFamily = 'Courier'

# get layout
layout1 = GetLayout()

# layout/tab size in pixels
layout1.SetSize(1113, 794)

# current camera placement for renderView1
renderView1.CameraPosition = [1.3293631534509824, -10.608530631730595, -1.1599311056317194]
renderView1.CameraFocalPoint = [1.3293631534509824, 6.343398821367176, -1.1599311056317194]
renderView1.CameraViewUp = [0.0, 0.0, 1.0]
renderView1.CameraParallelScale = 4.387482193696061

animationScene1.GoToLast()

# various visual improvements
glyph1.MaximumNumberOfSamplePoints = 1001
glyph1.GlyphType.TipLength = 0.55
glyph1.GlyphType.TipRadius = 0.2
glyph1.GlyphType.ShaftRadius = 0.06
glyph2.MaximumNumberOfSamplePoints = 1001
glyph2.GlyphType.TipLength = 0.55
glyph2.GlyphType.TipRadius = 0.2
glyph2.GlyphType.ShaftRadius = 0.06

# save screenshot
SaveScreenshot('./compare-XZ-slice.png', renderView1, ImageResolution=[1113, 794])

