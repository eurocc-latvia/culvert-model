#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# create a new 'OpenFOAMReader'
afoam = OpenFOAMReader(registrationName='a.foam', FileName='./3-simulations/a.foam')
afoam.MeshRegions = ['internalMesh']
afoam.CellArrays = ['U', 'alpha.water']

# get animation scene
animationScene1 = GetAnimationScene()

# get the time-keeper
timeKeeper1 = GetTimeKeeper()

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')

# get the material library
materialLibrary1 = GetMaterialLibrary()

# get display properties
afoamDisplay = GetDisplayProperties(afoam, view=renderView1)

# update animation scene based on data timesteps
animationScene1.UpdateAnimationUsingDataTimeSteps()

# create a new 'Slice'
slice1 = Slice(registrationName='Slice1', Input=afoam)
slice1.SliceType = 'Plane'
slice1.HyperTreeGridSlicer = 'Plane'
slice1.SliceOffsetValues = [0.0]

# init the 'Plane' selected for 'SliceType'
slice1.SliceType.Origin = [1.0, 0.0, 0.0]

# init the 'Plane' selected for 'HyperTreeGridSlicer'
slice1.HyperTreeGridSlicer.Origin = [1.0, 0.0, 0.0]

# show data in view
slice1Display = Show(slice1, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
slice1Display.Representation = 'Surface'
slice1Display.ColorArrayName = [None, '']
slice1Display.SelectTCoordArray = 'None'
slice1Display.SelectNormalArray = 'None'
slice1Display.SelectTangentArray = 'None'
slice1Display.OSPRayScaleArray = 'U'
slice1Display.OSPRayScaleFunction = 'PiecewiseFunction'
slice1Display.SelectOrientationVectors = 'U'
slice1Display.ScaleFactor = 0.12000000476837158
slice1Display.SelectScaleArray = 'None'
slice1Display.GlyphType = 'Arrow'
slice1Display.GlyphTableIndexArray = 'None'
slice1Display.GaussianRadius = 0.006000000238418579
slice1Display.SetScaleArray = ['POINTS', 'U']
slice1Display.ScaleTransferFunction = 'PiecewiseFunction'
slice1Display.OpacityArray = ['POINTS', 'U']
slice1Display.OpacityTransferFunction = 'PiecewiseFunction'
slice1Display.DataAxesGrid = 'GridAxesRepresentation'
slice1Display.PolarAxes = 'PolarAxesRepresentation'
slice1Display.SelectInputVectors = ['POINTS', 'U']
slice1Display.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
slice1Display.ScaleTransferFunction.Points = [-1.6894512176513672, 0.0, 0.5, 0.0, 0.6622717380523682, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
slice1Display.OpacityTransferFunction.Points = [-1.6894512176513672, 0.0, 0.5, 0.0, 0.6622717380523682, 1.0, 0.5, 0.0]

# hide data in view
Hide(afoam, renderView1)

# update the view to ensure updated data information
renderView1.Update()

# Properties modified on slice1.SliceType
slice1.SliceType.Origin = [0.001, 0.0, 0.0]

# update the view to ensure updated data information
renderView1.Update()

# toggle interactive widget visibility (only when running from the GUI)
HideInteractiveWidgets(proxy=slice1.SliceType)

renderView1.ResetActiveCameraToNegativeX()

# reset view to fit data
renderView1.ResetCamera(False)

# set scalar coloring
ColorBy(slice1Display, ('POINTS', 'U', 'Magnitude'))

# rescale color and/or opacity maps used to include current data range
slice1Display.RescaleTransferFunctionToDataRange(True, False)

# show color bar/color legend
slice1Display.SetScalarBarVisibility(renderView1, True)

# get color transfer function/color map for 'U'
uLUT = GetColorTransferFunction('U')

# get opacity transfer function/opacity map for 'U'
uPWF = GetOpacityTransferFunction('U')

# get 2D transfer function for 'U'
uTF2D = GetTransferFunction2D('U')

# Apply a preset using its name. Note this may not work as expected when presets have duplicate names.
uLUT.ApplyPreset('Viridis (matplotlib)', True)

# Properties modified on uLUT
uLUT.NumberOfTableValues = 24

# Properties modified on slice1.SliceType
slice1.SliceType.Origin = [0.003, 0.0, 0.0]

# update the view to ensure updated data information
renderView1.Update()

# Properties modified on slice1.SliceType
slice1.SliceType.Origin = [0.01, 0.0, 0.0]

# update the view to ensure updated data information
renderView1.Update()

# rescale color and/or opacity maps used to exactly fit the current data range
slice1Display.RescaleTransferFunctionToDataRange(False, True)

# Rescale transfer function
uLUT.RescaleTransferFunction(0.0, 2.5638507791229763)

# Rescale transfer function
uPWF.RescaleTransferFunction(0.0, 2.5638507791229763)

# Properties modified on slice1.SliceType
slice1.SliceType.Origin = [0.001, 0.0, 0.0]

# update the view to ensure updated data information
renderView1.Update()

# Rescale transfer function
uLUT.RescaleTransferFunction(0.0, 3.3611912538312065)

# Rescale transfer function
uPWF.RescaleTransferFunction(0.0, 3.3611912538312065)

# Rescale transfer function
uLUT.RescaleTransferFunction(0.0, 2.5853956230127895)

# Rescale transfer function
uPWF.RescaleTransferFunction(0.0, 2.5853956230127895)

# get color legend/bar for uLUT in view renderView1
uLUTColorBar = GetScalarBar(uLUT, renderView1)

# Properties modified on uLUTColorBar
uLUTColorBar.Title = 'v, m/s'
uLUTColorBar.ComponentTitle = ''
uLUTColorBar.RangeLabelFormat = '%-#6.2g'

# change scalar bar placement
uLUTColorBar.Orientation = 'Horizontal'
uLUTColorBar.WindowLocation = 'Any Location'
uLUTColorBar.Position = [0.32341419586702636, 0.889823677581864]
uLUTColorBar.ScalarBarLength = 0.3300000000000003

# create a new 'Contour'
contour1 = Contour(registrationName='Contour1', Input=slice1)
contour1.ContourBy = ['POINTS', 'alpha.water']
contour1.Isosurfaces = [0.4999923944014881]
contour1.PointMergeMethod = 'Uniform Binning'

# show data in view
contour1Display = Show(contour1, renderView1, 'GeometryRepresentation')

# get color transfer function/color map for 'alphawater'
alphawaterLUT = GetColorTransferFunction('alphawater')

# trace defaults for the display properties.
contour1Display.Representation = 'Surface'
contour1Display.ColorArrayName = ['POINTS', 'alpha.water']
contour1Display.LookupTable = alphawaterLUT
contour1Display.SelectTCoordArray = 'None'
contour1Display.SelectNormalArray = 'None'
contour1Display.SelectTangentArray = 'None'
contour1Display.OSPRayScaleArray = 'alpha.water'
contour1Display.OSPRayScaleFunction = 'PiecewiseFunction'
contour1Display.SelectOrientationVectors = 'U'
contour1Display.ScaleFactor = 0.09912484288215638
contour1Display.SelectScaleArray = 'alpha.water'
contour1Display.GlyphType = 'Arrow'
contour1Display.GlyphTableIndexArray = 'alpha.water'
contour1Display.GaussianRadius = 0.004956242144107819
contour1Display.SetScaleArray = ['POINTS', 'alpha.water']
contour1Display.ScaleTransferFunction = 'PiecewiseFunction'
contour1Display.OpacityArray = ['POINTS', 'alpha.water']
contour1Display.OpacityTransferFunction = 'PiecewiseFunction'
contour1Display.DataAxesGrid = 'GridAxesRepresentation'
contour1Display.PolarAxes = 'PolarAxesRepresentation'
contour1Display.SelectInputVectors = ['POINTS', 'U']
contour1Display.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
contour1Display.ScaleTransferFunction.Points = [0.49999240040779114, 0.0, 0.5, 0.0, 0.5001068711280823, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
contour1Display.OpacityTransferFunction.Points = [0.49999240040779114, 0.0, 0.5, 0.0, 0.5001068711280823, 1.0, 0.5, 0.0]

# hide data in view
Hide(slice1, renderView1)

# show color bar/color legend
contour1Display.SetScalarBarVisibility(renderView1, True)

# update the view to ensure updated data information
renderView1.Update()

# get opacity transfer function/opacity map for 'alphawater'
alphawaterPWF = GetOpacityTransferFunction('alphawater')

# get 2D transfer function for 'alphawater'
alphawaterTF2D = GetTransferFunction2D('alphawater')

# Properties modified on contour1
contour1.Isosurfaces = [0.3]

# update the view to ensure updated data information
renderView1.Update()

# Rescale transfer function
alphawaterLUT.RescaleTransferFunction(0.30000001192092896, 0.5001068711280823)

# Rescale transfer function
alphawaterPWF.RescaleTransferFunction(0.30000001192092896, 0.5001068711280823)

# turn off scalar coloring
ColorBy(contour1Display, None)

# Hide the scalar bar for this color map if no visible data is colored by it.
HideScalarBarIfNotNeeded(alphawaterLUT, renderView1)

# set active source
SetActiveSource(slice1)

# show data in view
slice1Display = Show(slice1, renderView1, 'GeometryRepresentation')

# show color bar/color legend
slice1Display.SetScalarBarVisibility(renderView1, True)

# update the view to ensure updated data information
renderView1.Update()

# Rescale transfer function
uLUT.RescaleTransferFunction(0.0, 3.3611912538312065)

# Rescale transfer function
uPWF.RescaleTransferFunction(0.0, 3.3611912538312065)

# set active source
SetActiveSource(contour1)

# Properties modified on contour1Display
contour1Display.LineWidth = 3.0

# set active source
SetActiveSource(slice1)

# set active source
SetActiveSource(contour1)

# Properties modified on contour1
contour1.Isosurfaces = [0.33]

# update the view to ensure updated data information
renderView1.Update()

# create a new 'OpenFOAMReader'
afoam_1 = OpenFOAMReader(registrationName='a.foam', FileName='./4-simulations-with-sHM-obstacle/a.foam')
afoam_1.MeshRegions = ['internalMesh']
afoam_1.CellArrays = ['U', 'alpha.water']

# show data in view
afoam_1Display = Show(afoam_1, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
afoam_1Display.Representation = 'Surface'
afoam_1Display.ColorArrayName = [None, '']
afoam_1Display.SelectTCoordArray = 'None'
afoam_1Display.SelectNormalArray = 'None'
afoam_1Display.SelectTangentArray = 'None'
afoam_1Display.OSPRayScaleArray = 'U'
afoam_1Display.OSPRayScaleFunction = 'PiecewiseFunction'
afoam_1Display.SelectOrientationVectors = 'U'
afoam_1Display.SelectScaleArray = 'None'
afoam_1Display.GlyphType = 'Arrow'
afoam_1Display.GlyphTableIndexArray = 'None'
afoam_1Display.GaussianRadius = 0.05
afoam_1Display.SetScaleArray = ['POINTS', 'U']
afoam_1Display.ScaleTransferFunction = 'PiecewiseFunction'
afoam_1Display.OpacityArray = ['POINTS', 'U']
afoam_1Display.OpacityTransferFunction = 'PiecewiseFunction'
afoam_1Display.DataAxesGrid = 'GridAxesRepresentation'
afoam_1Display.PolarAxes = 'PolarAxesRepresentation'
afoam_1Display.ScalarOpacityUnitDistance = 0.17601572776675842
afoam_1Display.OpacityArrayName = ['POINTS', 'U']
afoam_1Display.SelectInputVectors = ['POINTS', 'U']
afoam_1Display.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
afoam_1Display.ScaleTransferFunction.Points = [-2.5653088092803955, 0.0, 0.5, 0.0, 1.1079399585723877, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
afoam_1Display.OpacityTransferFunction.Points = [-2.5653088092803955, 0.0, 0.5, 0.0, 1.1079399585723877, 1.0, 0.5, 0.0]

# update the view to ensure updated data information
renderView1.Update()

# create a new 'Transform'
transform1 = Transform(registrationName='Transform1', Input=afoam_1)
transform1.Transform = 'Transform'

# show data in view
transform1Display = Show(transform1, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
transform1Display.Representation = 'Surface'
transform1Display.ColorArrayName = [None, '']
transform1Display.SelectTCoordArray = 'None'
transform1Display.SelectNormalArray = 'None'
transform1Display.SelectTangentArray = 'None'
transform1Display.OSPRayScaleArray = 'U'
transform1Display.OSPRayScaleFunction = 'PiecewiseFunction'
transform1Display.SelectOrientationVectors = 'U'
transform1Display.SelectScaleArray = 'None'
transform1Display.GlyphType = 'Arrow'
transform1Display.GlyphTableIndexArray = 'None'
transform1Display.GaussianRadius = 0.05
transform1Display.SetScaleArray = ['POINTS', 'U']
transform1Display.ScaleTransferFunction = 'PiecewiseFunction'
transform1Display.OpacityArray = ['POINTS', 'U']
transform1Display.OpacityTransferFunction = 'PiecewiseFunction'
transform1Display.DataAxesGrid = 'GridAxesRepresentation'
transform1Display.PolarAxes = 'PolarAxesRepresentation'
transform1Display.ScalarOpacityUnitDistance = 0.17601572776675842
transform1Display.OpacityArrayName = ['POINTS', 'U']
transform1Display.SelectInputVectors = ['POINTS', 'U']
transform1Display.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
transform1Display.ScaleTransferFunction.Points = [-2.5653088092803955, 0.0, 0.5, 0.0, 1.1079399585723877, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
transform1Display.OpacityTransferFunction.Points = [-2.5653088092803955, 0.0, 0.5, 0.0, 1.1079399585723877, 1.0, 0.5, 0.0]

# hide data in view
Hide(afoam_1, renderView1)

# update the view to ensure updated data information
renderView1.Update()

# Properties modified on transform1.Transform
transform1.Transform.Translate = [0.0, 1.5, 0.0]

# update the view to ensure updated data information
renderView1.Update()

# create a new 'Slice'
slice2 = Slice(registrationName='Slice2', Input=transform1)
slice2.SliceType = 'Plane'
slice2.HyperTreeGridSlicer = 'Plane'
slice2.SliceOffsetValues = [0.0]

# init the 'Plane' selected for 'SliceType'
slice2.SliceType.Origin = [0.0, 1.5, 0.0]

# init the 'Plane' selected for 'HyperTreeGridSlicer'
slice2.HyperTreeGridSlicer.Origin = [0.0, 1.5, 0.0]

# show data in view
slice2Display = Show(slice2, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
slice2Display.Representation = 'Surface'
slice2Display.ColorArrayName = [None, '']
slice2Display.SelectTCoordArray = 'None'
slice2Display.SelectNormalArray = 'None'
slice2Display.SelectTangentArray = 'None'
slice2Display.OSPRayScaleArray = 'U'
slice2Display.OSPRayScaleFunction = 'PiecewiseFunction'
slice2Display.SelectOrientationVectors = 'U'
slice2Display.ScaleFactor = 0.30000000000000004
slice2Display.SelectScaleArray = 'None'
slice2Display.GlyphType = 'Arrow'
slice2Display.GlyphTableIndexArray = 'None'
slice2Display.GaussianRadius = 0.015
slice2Display.SetScaleArray = ['POINTS', 'U']
slice2Display.ScaleTransferFunction = 'PiecewiseFunction'
slice2Display.OpacityArray = ['POINTS', 'U']
slice2Display.OpacityTransferFunction = 'PiecewiseFunction'
slice2Display.DataAxesGrid = 'GridAxesRepresentation'
slice2Display.PolarAxes = 'PolarAxesRepresentation'
slice2Display.SelectInputVectors = ['POINTS', 'U']
slice2Display.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
slice2Display.ScaleTransferFunction.Points = [-2.2700257301330566, 0.0, 0.5, 0.0, 0.5298245549201965, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
slice2Display.OpacityTransferFunction.Points = [-2.2700257301330566, 0.0, 0.5, 0.0, 0.5298245549201965, 1.0, 0.5, 0.0]

# hide data in view
Hide(transform1, renderView1)

# update the view to ensure updated data information
renderView1.Update()

# Properties modified on slice2.SliceType
slice2.SliceType.Origin = [0.001, 1.5, 0.0]

# update the view to ensure updated data information
renderView1.Update()

# toggle interactive widget visibility (only when running from the GUI)
HideInteractiveWidgets(proxy=slice2.SliceType)

# set scalar coloring
ColorBy(slice2Display, ('POINTS', 'U', 'Magnitude'))

# rescale color and/or opacity maps used to include current data range
slice2Display.RescaleTransferFunctionToDataRange(True, False)

# show color bar/color legend
slice2Display.SetScalarBarVisibility(renderView1, True)

# Properties modified on uLUT
uLUT.NumberOfTableValues = 18

# create a new 'Contour'
contour2 = Contour(registrationName='Contour2', Input=slice2)
contour2.ContourBy = ['POINTS', 'alpha.water']
contour2.Isosurfaces = [0.500081775317085]
contour2.PointMergeMethod = 'Uniform Binning'

# show data in view
contour2Display = Show(contour2, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
contour2Display.Representation = 'Surface'
contour2Display.ColorArrayName = ['POINTS', 'alpha.water']
contour2Display.LookupTable = alphawaterLUT
contour2Display.SelectTCoordArray = 'None'
contour2Display.SelectNormalArray = 'None'
contour2Display.SelectTangentArray = 'None'
contour2Display.OSPRayScaleArray = 'alpha.water'
contour2Display.OSPRayScaleFunction = 'PiecewiseFunction'
contour2Display.SelectOrientationVectors = 'U'
contour2Display.ScaleFactor = 0.10081841349601746
contour2Display.SelectScaleArray = 'alpha.water'
contour2Display.GlyphType = 'Arrow'
contour2Display.GlyphTableIndexArray = 'alpha.water'
contour2Display.GaussianRadius = 0.005040920674800873
contour2Display.SetScaleArray = ['POINTS', 'alpha.water']
contour2Display.ScaleTransferFunction = 'PiecewiseFunction'
contour2Display.OpacityArray = ['POINTS', 'alpha.water']
contour2Display.OpacityTransferFunction = 'PiecewiseFunction'
contour2Display.DataAxesGrid = 'GridAxesRepresentation'
contour2Display.PolarAxes = 'PolarAxesRepresentation'
contour2Display.SelectInputVectors = ['POINTS', 'U']
contour2Display.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
contour2Display.ScaleTransferFunction.Points = [0.5000817775726318, 0.0, 0.5, 0.0, 0.5002038478851318, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
contour2Display.OpacityTransferFunction.Points = [0.5000817775726318, 0.0, 0.5, 0.0, 0.5002038478851318, 1.0, 0.5, 0.0]

# hide data in view
Hide(slice2, renderView1)

# show color bar/color legend
contour2Display.SetScalarBarVisibility(renderView1, True)

# update the view to ensure updated data information
renderView1.Update()

# Properties modified on contour2
contour2.Isosurfaces = [0.33]

# update the view to ensure updated data information
renderView1.Update()

# set active source
SetActiveSource(slice2)

# show data in view
slice2Display = Show(slice2, renderView1, 'GeometryRepresentation')

# show color bar/color legend
slice2Display.SetScalarBarVisibility(renderView1, True)

# update the view to ensure updated data information
renderView1.Update()

# set active source
SetActiveSource(contour2)

# turn off scalar coloring
ColorBy(contour2Display, None)

# Hide the scalar bar for this color map if no visible data is colored by it.
HideScalarBarIfNotNeeded(alphawaterLUT, renderView1)

# Properties modified on contour2Display
contour2Display.LineWidth = 3.0

# create a new 'Text'
text1 = Text(registrationName='Text1')

# show data in view
text1Display = Show(text1, renderView1, 'TextSourceRepresentation')

# update the view to ensure updated data information
renderView1.Update()

# Properties modified on text1
text1.Text = 'Without obstacle'

# update the view to ensure updated data information
renderView1.Update()

# Properties modified on text1Display
text1Display.FontFamily = 'Courier'

# Properties modified on text1Display
text1Display.WindowLocation = 'Lower Left Corner'

# create a new 'Text'
text2 = Text(registrationName='Text2')

# show data in view
text2Display = Show(text2, renderView1, 'TextSourceRepresentation')

# update the view to ensure updated data information
renderView1.Update()

# Properties modified on text2
text2.Text = 'With obstacle'

# update the view to ensure updated data information
renderView1.Update()

# Properties modified on text2Display
text2Display.WindowLocation = 'Lower Right Corner'

# Properties modified on text2Display
text2Display.FontFamily = 'Courier'

# set active source
SetActiveSource(slice1)

# update the view to ensure updated data information
renderView1.Update()

# Rescale transfer function
uLUT.RescaleTransferFunction(0.0, 2.5)

# Rescale transfer function
uPWF.RescaleTransferFunction(0.0, 2.5)

# Rescale 2D transfer function
uTF2D.RescaleTransferFunction(0.0, 2.5, 0.0, 1.0)

# set active source
SetActiveSource(slice2)

# get layout
layout1 = GetLayout()

# layout/tab size in pixels
layout1.SetSize(1113, 794)

# current camera placement for renderView1
renderView1.CameraPosition = [3.9161691357670887, 0.7033148535879781, 0.033685023796478]
renderView1.CameraFocalPoint = [0.6377080364101025, 0.7033148535879781, 0.033685023796478]
renderView1.CameraViewUp = [0.0, 0.0, 1.0]
renderView1.CameraParallelScale = 0.8485281711413358


# change scalar bar placement
uLUTColorBar.Position = [0.3611500449236301, 0.8936020151133501]
uLUTColorBar.ScalarBarLength = 0.3300000000000003

animationScene1.GoToLast()

# save screenshot
SaveScreenshot('./compare-YZ-slice-exit.png', renderView1, ImageResolution=[1113, 794])
