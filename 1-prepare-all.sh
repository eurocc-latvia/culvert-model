# module load singularity
# module load openfoam/openfoam-v2312
# source /software/openfoam/OpenFOAM-v2312/etc/bashrc
python3 set-parameters.py

echo $'\n\n\n\n\n\nINFO: Making the cylinder mesh\n\n';
cd 0-cylinder
m4 cylinderMesh.m4 >system/blockMeshDict
blockMesh
transformPoints -rotate-z -90
foamToVTK
cd ..

echo $'\n\n\n\n\n\nINFO: Making the cube mesh\n\n';
cd 1-cube
blockMesh
topoSet -dict system/topoSetDict_refine
refineMesh -dict system/refineMeshDict -overwrite
foamToVTK
cd ..

echo $'\n\n\n\n\n\nINFO: Connecting the meshes\n\n';
cd 2-cube-and-cylinder
rm -rf 0 constant
cp -r ../1-cube/constant .
topoSet -dict system/topoSetDict_couple
createPatch -overwrite -dict system/createPatchDict_couple
mergeMeshes . ../0-cylinder -overwrite
stitchMesh intersectionPatch intersectionCylinderPatch -partial -overwrite
# set the fixed mass flow with automatically changing height
checkMesh
cd ..

echo $'\n\n\n\n\n\nINFO: Preparing initial and boundary conditions\n\n';
cd 3-simulations
rm -rf constant/polyMesh 0
rm -rf 1* 2* 3* 4* 5* 6* 7* 8* 9* 10* processor* log* slurm* VTK
cp -r ../2-cube-and-cylinder/constant/polyMesh constant
cp -r 0.orig 0
setFields # water level initial conditions should be calculated from mass flow
foamToVTK
cd ..

echo $'\n\n\n\n\n\nINFO: Creating an obstacle\n\n';
cd 4-simulations-with-sHM-obstacle
rm -rf 0* 1* 2* 3* 4* 5* 6* 7* 8* 9* 10* processor* log* slurm* VTK constant
cp -r ../3-simulations/* .
rm -rf VTK
cp ../3-simulations/0.orig/* 0
mkdir constant/triSurface
singularity exec OpenSCAD_container.sif openscad obstacle.scad --render -o constant/triSurface/obstacle.stl
snappyHexMesh -overwrite
setFields # water level initial conditions should be calculated from mass flow
foamToVTK
checkMesh
cd ..
pvpython draw-mesh.py

echo $'\n\n\n\n\n\nINFO: All pre-processing is done!\n\n';

### old, ugly obstacle:
# topoSet
# subsetMesh -overwrite c0 -patch sideCylinderWall
# setFields
