#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# create a new 'OpenFOAMReader'
afoam = OpenFOAMReader(registrationName='a.foam', FileName='./4-simulations-with-sHM-obstacle/a.foam')
afoam.MeshRegions = ['internalMesh']
afoam.CellArrays = []

# get animation scene
animationScene1 = GetAnimationScene()

# get the time-keeper
timeKeeper1 = GetTimeKeeper()

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')
renderView1.AxesGrid.Visibility = 1

# get the material library
materialLibrary1 = GetMaterialLibrary()

# get display properties
afoamDisplay = GetDisplayProperties(afoam, view=renderView1)

# update animation scene based on data timesteps
animationScene1.UpdateAnimationUsingDataTimeSteps()

# Properties modified on renderView1.AxesGrid
renderView1.AxesGrid.XTitle = ''
renderView1.AxesGrid.YTitle = ''
renderView1.AxesGrid.ZTitle = ''
renderView1.AxesGrid.XAxisUseCustomLabels = 0
renderView1.AxesGrid.XAxisLabels = []

# change representation type
afoamDisplay.SetRepresentationType('Surface With Edges')

# get layout
layout1 = GetLayout()

# split cell
layout1.SplitHorizontal(0, 0.5)

# set active view
SetActiveView(None)

# Create a new 'Render View'
renderView2 = CreateView('RenderView')
renderView2.AxesGrid = 'GridAxes3DActor'
renderView2.StereoType = 'Crystal Eyes'
renderView2.CameraFocalDisk = 1.0
renderView2.BackEnd = 'OSPRay raycaster'
renderView2.OSPRayMaterialLibrary = materialLibrary1

# assign view to a particular cell in the layout
AssignViewToLayout(view=renderView2, layout=layout1, hint=2)

# split cell
layout1.SplitVertical(2, 0.5)

# set active view
SetActiveView(None)

# Create a new 'Render View'
renderView3 = CreateView('RenderView')
renderView3.AxesGrid = 'GridAxes3DActor'
renderView3.StereoType = 'Crystal Eyes'
renderView3.CameraFocalDisk = 1.0
renderView3.BackEnd = 'OSPRay raycaster'
renderView3.OSPRayMaterialLibrary = materialLibrary1

# assign view to a particular cell in the layout
AssignViewToLayout(view=renderView3, layout=layout1, hint=6)

# set active view
SetActiveView(renderView1)

# split cell
layout1.SplitVertical(1, 0.5)

# set active view
SetActiveView(None)

# Create a new 'Render View'
renderView4 = CreateView('RenderView')
renderView4.AxesGrid = 'GridAxes3DActor'
renderView4.StereoType = 'Crystal Eyes'
renderView4.CameraFocalDisk = 1.0
renderView4.BackEnd = 'OSPRay raycaster'
renderView4.OSPRayMaterialLibrary = materialLibrary1

# assign view to a particular cell in the layout
AssignViewToLayout(view=renderView4, layout=layout1, hint=4)

# set active view
SetActiveView(renderView1)

# set active view
SetActiveView(renderView2)

# set active source
SetActiveSource(afoam)

# show data in view
afoamDisplay_1 = Show(afoam, renderView2, 'UnstructuredGridRepresentation')

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
afoamDisplay_1.ScaleTransferFunction.Points = [-2.5653088092803955, 0.0, 0.5, 0.0, 1.1079399585723877, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
afoamDisplay_1.OpacityTransferFunction.Points = [-2.5653088092803955, 0.0, 0.5, 0.0, 1.1079399585723877, 1.0, 0.5, 0.0]

# reset view to fit data
renderView2.ResetCamera(False)

# update the view to ensure updated data information
renderView2.Update()

# update the view to ensure updated data information
renderView3.Update()

# change representation type
afoamDisplay_1.SetRepresentationType('Surface With Edges')

renderView2.ResetActiveCameraToNegativeX()

# reset view to fit data
renderView2.ResetCamera(False)

# Properties modified on renderView2
renderView2.CameraParallelProjection = 1

# set active view
SetActiveView(renderView4)

renderView4.ResetActiveCameraToPositiveY()

# reset view to fit data
renderView4.ResetCamera(False)

# show data in view
afoamDisplay_2 = Show(afoam, renderView4, 'UnstructuredGridRepresentation')

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
afoamDisplay_2.ScaleTransferFunction.Points = [-2.5653088092803955, 0.0, 0.5, 0.0, 1.1079399585723877, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
afoamDisplay_2.OpacityTransferFunction.Points = [-2.5653088092803955, 0.0, 0.5, 0.0, 1.1079399585723877, 1.0, 0.5, 0.0]

# reset view to fit data
renderView4.ResetCamera(False)

# change representation type
afoamDisplay_2.SetRepresentationType('Surface With Edges')

# set active view
SetActiveView(renderView3)

# show data in view
afoamDisplay_3 = Show(afoam, renderView3, 'UnstructuredGridRepresentation')

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
afoamDisplay_3.ScaleTransferFunction.Points = [-2.5653088092803955, 0.0, 0.5, 0.0, 1.1079399585723877, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
afoamDisplay_3.OpacityTransferFunction.Points = [-2.5653088092803955, 0.0, 0.5, 0.0, 1.1079399585723877, 1.0, 0.5, 0.0]

# reset view to fit data
renderView3.ResetCamera(False)

renderView3.ApplyIsometricView()

# reset view to fit data
renderView3.ResetCamera(False)

# change representation type
afoamDisplay_3.SetRepresentationType('Surface With Edges')

# Properties modified on renderView3.AxesGrid
renderView3.AxesGrid.Visibility = 1

# set active view
SetActiveView(renderView4)

# Properties modified on renderView4.AxesGrid
renderView4.AxesGrid.Visibility = 1

# set active view
SetActiveView(renderView2)

# Properties modified on renderView2.AxesGrid
renderView2.AxesGrid.Visibility = 1

# layout/tab size in pixels
layout1.SetSize(1104, 759)

# current camera placement for renderView1
renderView1.CameraPosition = [0.0, 0.0, 17.116698261182588]
renderView1.CameraFocalPoint = [0.0, 0.0, -3.419166494295421]
renderView1.CameraParallelScale = 5.315072906367325

# current camera placement for renderView2
renderView2.CameraPosition = [9.31017848194687, 0.0, 0.0]
renderView2.CameraFocalPoint = [-11.225686273531126, 0.0, 0.0]
renderView2.CameraViewUp = [0.0, 0.0, 1.0]
renderView2.CameraParallelScale = 2.6457949882063403
renderView2.CameraParallelProjection = 1

# current camera placement for renderView3
renderView3.CameraPosition = [6.614959341539608, -13.533109473237852, 11.028048451949061]
renderView3.CameraFocalPoint = [-0.6615979534442339, 1.3535196618679517, -1.1029749254052412]
renderView3.CameraViewUp = [-0.24929014933088303, 0.5356224189565633, 0.8068228093935532]
renderView3.CameraParallelScale = 5.315072906367325

# current camera placement for renderView4
renderView4.CameraPosition = [0.0, -17.90695496540989, 0.0]
renderView4.CameraFocalPoint = [0.0, 2.628909790068105, 0.0]
renderView4.CameraViewUp = [0.0, 0.0, 1.0]
renderView4.CameraParallelScale = 5.315072906367325

# save screenshot
SaveScreenshot('./mesh.png', layout1, SaveAllViews=1,
    ImageResolution=[1104, 759])
