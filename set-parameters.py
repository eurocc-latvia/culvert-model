from parameters import *
from replacing import *


# Checking topology of the room
if culvert_height<0:
    print('WARNING: [culvert_height] should be greater than zero')
if reservoir_height<culvert_diameter:
    print('WARNING: [reservoir_height] should be greater than [culvert_diameter]')
if obstacle_x>(culvert_length-obstacle_r):
    print('WARNING: obstacle should be inside the pipe ([obstacle_x] should be less than [culvert_length-obstacle_r]')
    
# Creating geometry of the cylinder
print('Creating geometry of the cylinder...')
stringToInsert = '   define(D, {0}) // culvert diameter\n'.format(culvert_diameter)
replaceIn('0-cylinder/cylinderMesh.m4', 'diameter', stringToInsert)
stringToInsert = '   define(L, {0}) // culvert length\n'.format(culvert_length)
replaceIn('0-cylinder/cylinderMesh.m4', 'length', stringToInsert)
stringToInsert = '   define(NPS, {0}) // how many cells in the square section - N_sq_cylinder\n'.format(N_sq_cylinder)
replaceIn('0-cylinder/cylinderMesh.m4', 'N_sq_cylinder', stringToInsert)
stringToInsert = '   define(NPD, {0}) // how many cells from square section to perimeter - N_out_cylinder\n'.format(N_out_cylinder)
replaceIn('0-cylinder/cylinderMesh.m4', 'N_out_cylinder', stringToInsert)
stringToInsert = '   define(NPY, {0}) // how many cells along the x direction - n_x_cylinder\n'.format(int(n_x_cylinder*culvert_length))
replaceIn('0-cylinder/cylinderMesh.m4', 'n_x_cylinder', stringToInsert)

stringToInsert = '            ( 0.001 0 {0} ) // pr_middle\n'.format(-culvert_diameter/2+0.1)
replaceIn('3-simulations/system/controlDict', 'pr_middle', stringToInsert)


# Creating geometry of the reservoir
print('Creating geometry of the reservoir: changing 1-cube/system/blockMeshDict...')
fOut = open('1-cube/system/blockMeshDict', 'w')
fOut.write(blockMeshDictHeader)

z_bottom = -culvert_diameter/2 - culvert_height
z_top = z_bottom + reservoir_height
fOut.write('\t({0}   {1}   {2})\n'.format(-reservoir_length, -reservoir_width/2, z_bottom))
fOut.write('\t({0}   {1}   {2})\n'.format(0,                 -reservoir_width/2, z_bottom))
fOut.write('\t({0}   {1}   {2})\n'.format(0,                  reservoir_width/2, z_bottom))
fOut.write('\t({0}   {1}   {2})\n'.format(-reservoir_length,  reservoir_width/2, z_bottom))
fOut.write('\t({0}   {1}   {2})\n'.format(-reservoir_length, -reservoir_width/2, z_top))
fOut.write('\t({0}   {1}   {2})\n'.format(0,                 -reservoir_width/2, z_top))
fOut.write('\t({0}   {1}   {2})\n'.format(0,                  reservoir_width/2, z_top))
fOut.write('\t({0}   {1}   {2})\n'.format(-reservoir_length,  reservoir_width/2, z_top))
fOut.write(""");

blocks
(
    hex (0 1 2 3 4 5 6 7) """)
fOut.write('({0} {1} {2})\n'.format(
    int(n_x_reservoir*reservoir_length),
    int(n_y_reservoir*reservoir_width),
    int(n_z_reservoir*reservoir_height)
))
fOut.write(blockMeshDictFooter)
fOut.close()

stringToInsert = '            radius   {0};\n'.format(culvert_diameter/2 + 0.15)
replaceIn('1-cube/system/topoSetDict_refine', 'radius', stringToInsert)
stringToInsert = '            radius   {0};\n'.format(culvert_diameter/2 + 0.05)
replaceIn('2-cube-and-cylinder/system/topoSetDict_couple', 'radius', stringToInsert)


# Creating initial and boundary conditions
print('Creating initial and boundary conditions...')
stringToInsert = '        flowRate        {0}; // in m^3/s\n'.format(flow_rate)
replaceIn('3-simulations/0.orig/U', 'flowRate', stringToInsert)
stringToInsert = '            (0 {0} {1}) ({2} {3} {4}) // box_1\n'.format(-culvert_diameter/2, -culvert_diameter/2, culvert_length, culvert_diameter/2, -culvert_diameter/2+initial_water_level)
replaceIn('3-simulations/system/setFieldsDict', 'box_1', stringToInsert)
stringToInsert = '            ({0} {1} {2}) (0 {3} {4}) // box_2\n'.format(-reservoir_length, -reservoir_width/2, z_bottom, reservoir_width/2, z_bottom+culvert_height)
replaceIn('3-simulations/system/setFieldsDict', 'box_2', stringToInsert)


# Creating the obstacle
print('Creating the obstacle...')
fOut = open('4-simulations-with-sHM-obstacle/obstacle.scad', 'w')
fOut.write('$fn=25;\n')
fOut.write('translate([{0}, 2, {1}]) rotate([90, 0, 0]) cylinder(h=4, r={2});'.format(obstacle_x, -culvert_diameter/2, obstacle_r))
fOut.close()

stringToInsert = '        min  ( {0} {1} {2}); // min_box\n'.format(obstacle_x-0.3, -culvert_diameter/2, -culvert_diameter/2)
replaceIn('3-simulations/system/snappyHexMeshDict', 'min_box', stringToInsert)
stringToInsert = '        max  ( {0} {1} {2}); // max_box\n'.format(obstacle_x+0.3, culvert_diameter/2, -culvert_diameter/2+obstacle_r+0.2)
replaceIn('3-simulations/system/snappyHexMeshDict', 'max_box', stringToInsert)


# Setting the number of cores
stringToInsert = '#SBATCH --ntasks={0}                      # Number of tasks\n'.format(n_proc)
replaceIn('3-simulations/run-slurm.sh', 'ntasks', stringToInsert)
stringToInsert = 'mpirun -np {0} interFoam -parallel >./log-run\n'.format(n_proc)
replaceIn('3-simulations/run-slurm.sh', 'mpirun', stringToInsert)
stringToInsert = 'numberOfSubdomains {0};\n'.format(n_proc)
replaceIn('3-simulations/system/decomposeParDict', 'numberOfSubdomains', stringToInsert)

