#!/bin/bash
#SBATCH --job-name=EuroCC-demo_culvert   # Job name
#SBATCH --ntasks=8                      # Number of tasks
#SBATCH --partition=regular              # Partition name
#SBATCH --mem-per-cpu=2G                 # Memory per cpu-core
#SBATCH --time=694:00:00                 # Time limit hrs:min:sec

echo "Current dir = "; pwd; hostname; date
decomposePar -force
mpirun -np 8 interFoam -parallel >./log-run
echo ""; echo ""; echo ""; echo "" 
echo "Simulations are completed! Current time:"
date

echo ""; echo ""; echo ""; echo "" 
echo "Drawing results:"
reconstructPar -fields "(U alpha.water)"
cd ..
./3-draw-results.sh
