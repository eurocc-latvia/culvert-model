#! /bin/sh

set terminal pngcairo enhanced font "Century Schoolbook, 13" size 950,450
set output "v-without-obstacle.png"
set grid
set autoscale
set title "Velocity at the end of the culvert, near its bottom"
set ylabel 'v, m/s'
set xlabel 't, s'
set style line 1 
set key outside right
plot "< cat 3-simulations/postProcessing/probe_middle/0/U | tr -d '()' " using 1:2 lw 3 title 'x-component' with lines,\
	 "< cat 3-simulations/postProcessing/probe_middle/0/U | tr -d '()'" using 1:3 lw 3 title 'y-component' with lines,\
     "< cat 3-simulations/postProcessing/probe_middle/0/U | tr -d '()'" using 1:4 lw 3 title 'z-component' with lines,


set output "v-with-obstacle.png"
set title "Velocity at the end of the culvert, near its bottom"
set ylabel 'v, m/s'
set xlabel 't, s'
set style line 1 
set key outside right
plot "< cat 4-simulations-with-sHM-obstacle/postProcessing/probe_middle/0/U | tr -d '()' " using 1:2 lw 3 title 'x-component' with lines,\
	 "< cat 4-simulations-with-sHM-obstacle/postProcessing/probe_middle/0/U | tr -d '()'" using 1:3 lw 3 title 'y-component' with lines,\
     "< cat 4-simulations-with-sHM-obstacle/postProcessing/probe_middle/0/U | tr -d '()'" using 1:4 lw 3 title 'z-component' with lines,

