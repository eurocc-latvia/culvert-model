# Summary

This demonstration case models non-stationary water flow through a culvert. Mass inflow is set at the inlet, and the water level is adjusted automatically depending on mass inflow and system geometry. Free flow condition -- zero gradient of velocity -- is used at the outlet.

Several simplifications are made. The culvert is assumed to be perfectly cylindrical. For the water flow, *k-&epsilon;* turbulence model is used. Both mesh generation and physics simulations are performed using the *[OpenFOAM](https://openfoam.org/)* library. For the preparation of the obstacle geometry *stl* file, *[OpenSCAD](https://openscad.org/)* is used. For convenience of first-time users, mesh preparation and other pre-processing is automated using *shell* scripts.

The purpose of this demonstration is to show *OpenFOAM* capabilities in modelling the three-dimensional two-phase flow (water and air) in the culvert, and to evaluate the influence of obstacles on water velocity distribution. In addition, this demonstration showcases the typical main steps of HPC simulations.

**This HPC demonstration was prepared as a part of the training and dissemination activities organized by the [EuroCC National HPC Competence Centre of Latvia](https://eurocc-latvia.lv/?lang=en).**


# Table of contents
[[_TOC_]]

# Problem background
This case considers a cylindrical culvert, which opens into a larger rectangular reservouir, representing the free-stream on the other side of the culvert. Mass flux of water is set at the inlet of the culvert on the right, see Figure below. The water level and inlet velocity are determined automatically.

To get visualizations of the obtained results, *[ParaView](https://www.paraview.org/download/)* program is required. However, simpler vizualization of results is also performed automatically after the calculations, see Section [Vizualization of obtained results](#running-water-flow-simulations).

<img src="img_README/compare-XZ-slice.png" width="650" alt="Example of obtained results: water velocity in the longitudinal vertical slice of the culvert">

![Animation of water movement](img_README/video.gif)

Default culvert parameters are length of 3 m and radius of 0.6 m. The desigations of system geometrical parameters are shown in the image below. These parameters can be changed in `parameters.py` file, see [Mesh generation](#mesh-generation).

![Geometrical parameters of the culvert](img_README/sketch.png)

Boundary conditions for velocity (the same velocity field *U* is used for both air and water) are:
- Fixed mass flux condition `variableHeightFlowRateInletVelocity`, which automatically adjusts water level, at the culvert inlet (the rightmost boundary)
- The `inletOutlet` condition (equivalent to zero gradient, or free outflow) at the left boundary of the reservoir
- The `pressureInletOutletVelocity` condition (meaning the free excahnge of air with the atmosphere) at the upper boundary of the reservoir
- No-slip condition at all other surfaces

There is also a field containing mass fraction of water (*&alpha;* = 0 denotes air, *&alpha;* = 0 denotes water) with the following boundary conditions:
- Fixed mass flux condition `variableHeightFlowRate` at the culvert inlet (the rightmost boundary)
- The `inletOutlet` condition at the left and top boundaries of the reservoir
- Zero-gradient condition at all other surfaces


# Contents of this case
Here is the overview of all files in this repository. To run this simulation, it is *not* necessary to understand the meaning of each file in details.
* `parameters.py` — the file that contains main user-defined geometrical, physical and mesh-related parameters of the simulation.
* folders for *OpenFOAM* mesh creation: `0-cylinder`, `1-cube`, `2-cube-and-cylinder`
* folders for *OpenFOAM* simulations: `3-simulations`, `4-simulations-with-sHM-obstacle`, each containing the following files
  * `0` — Initial conditions for velocity, pressure and mass fraction
  * `constant` — Material properties and mesh
  * `system` — Various parameters of *OpenFOAM* calculations, pre- and post-processing options
    * `controlDict` — Time step, calculation lenght and other calculation controls
    * `fvSchemes`, `fvSolution` — Parameters of solvers and numerical schemes
    * `blockMeshDict`, `snappyHexMeshDict` — Instructions for mesh generation
  * `run-slurm.sh` — Batch script for submitting the simulation job to an HPC cluster where [*Slurm*](https://slurm.schedmd.com/sbatch.html) workload manager is installed
  * `OpenSCAD_container.sif` — *singularity* container with *OpenSCAD* program for the generation of `4-simulations-with-sHM-obstacle/constant/triSurface/obstacle.stl` file, which is then used by `snappyHexMesh`
* `.sh` scripts — scripts that prepare and run simulations 
* `.py` scripts — scripts that are invoked by `.sh` scripts to define the geometry of the domain, boundary conditions, and to draw the results
* `README.md` — This *readme* file
* `img_README` — Folder containing images for this *readme* file




# Running the case on an HPC cluster
Use the instruction from this chapter to run simulations **without** root rights. In this case all necessary modules (*conda*, *OpenFOAM*, *singularity*) should **already be installed**. This requriement is satisfied at LU and RTU clusters. For other clusters, **at least** *singularity* software should be installed.

## Obtaining the case

To be able to connect to HPC cluster, you need to download and install appropriate software, for example:
* [*PuTTy*](https://www.putty.org/) to execute commands in terminal (command line)
* [*WinScp*](https://winscp.net/eng/download.php) to view and copy files

After these programs are installed, open each of them and log in using your username and password. Then download this calculation case directly from GitLab by executing the command below.
```
git clone https://gitlab.com/eurocc-latvia/culvert-model.git
```
Alternatively, the code can be downloaded as `.zip` or `.tar` archive, copied to cluster by *WinScp*, and then un-archived by `unzip` or `tar -xzvf`.

## Preparation of the environment

**Loading software modules**

Navigate to the main directory of the case and change permissions of `.sh` scripts:
```
cd culvert-model
curl https://gitlab.com/eurocc-latvia/culvert-model/-/raw/main/4-simulations-with-sHM-obstacle/OpenSCAD_container.sif?ref_type=heads --output 4-simulations-with-sHM-obstacle/OpenSCAD_container.sif
chmod +x *.sh
```

Execute the following command to load modules that are pre-installed on the cluster:
```
source 0-prepare-environment.sh
```

**If** any of the required programs (*openfoam-v2312*, *singularity*, *paraview*) is not found, then
1. Exit the `source` process by pressing *Ctrl-C*
2. Contact your system administrator and ask to install them all. After all programs are installed, repeat the `source 0-prepare-environment.sh` command

The sourcing of the working environment by running `source 0-prepare-environment.sh` should be performed *after every login*.

## Mesh generation and boundary conditions
The main geometrical parameters of the case, as well as some mesh parameters, are defined in `parameters.py` file. The meaning of the variable names is given in [Problem background](#problem-background). The inlet flow rate of water, initail water level, as well as number of processors to use for simulations, are also defined in `parameters.py` file. The provided water level serves only as the initial condition, and the resulting water level is calculated by *OpenFOAM*. To prepare the mesh using these parameters, execute the following command:
```
./1-prepare-all.sh
```
Execution of this command may take from several minutes to one hour or more, depending on geometrical parameters and mesh size. After the mesh is ready, `mesh-*.png` files will appear in the current directory. They show mesh slices in three planes (*xz*, *yz* and *xy*) and allow to check if the mesh looks good before continuing to the next step. An example of these images is shown below (here coarse mesh is shown, default parameters should produce finer mesh):

![An example of created mesh](img_README/mesh.png)

After the command is completed, the obtained mesh can be viewed in *[ParaView](https://www.paraview.org/download/)* by opening the files in the `VTK` folder. Statistics about the mesh quality is written at the end of the `log-prepare` file. Small problems with the mesh (300-500 highly non-orthogonal faces, several highly skewed faces) likely may not obstruct simulations. If the problems are more severe, try to run the simulation (as described in the next section) anyway. If the simulation crashes, change mesh parameters in `parameters.py`. The additional mesh refinement levels are given in `4-simulations-with-sHM-obstacle/system/snappyHexMeshDict` file, line 141.

## Running water flow simulations
To run air flow simulation on a cluster where *slurm* job manager is installed, simply type:
```
./2-run-simulation.sh
```
The calculation time depends on the mesh size and processor number. It can take from several minutes to several hours. By default, calculations will be run in parallel, the number of processors is defined in the last line of `parameters.py`. It is possible to run simulations without *slurm*, in this case you should manually enter the simulation directory and run `chmod +x run-slurm.sh && ./run-slurm.sh >log-run &`.

**Simulation control**

Simulation will automatically end if `endTime`, defined in `system/controlDict` file, is reached. This file can be modified while simulation is still running. 

When the simulation **is completed**, the corresponding message will appear in the end of `slurm-...` logfile (if *slurm* was used to run the job, e.g. on LU cluster) or `log-run` logfile (if the job was run witout *slurm*), and the results visualisation process will be automatically started, creating `.png` and `VTK` files in the simulation directories. Note that the images in `v-*.png` show the *projection* of velocity on the corresponding plane, not full velocity magnitude.

Additional information about simulation progress is provided in `log-run` file. It contains *OpenFOAM* output: reporting of solver preparation, physical parameters, and residuals at each timestep. However, more convenient way of monitoring simulation progress is described in the next section.

**Vizualization of simulation progress**

To check calculation progress before it is completed, execute
```
gnuplot plot-probes.gnu
```
Two `v-....png` files with time-dependencies of velocity components in the cases with and without obstacle will be created. Probe locations are defined at the end of `system/controlDict` file, and by default they are put at the end of the culvert, near its bottom. During the beginning, the results are not realistic, because they depend on the initial condition. The soulution should converge to steady-state at the end, however the convergence time depends on the size of the system. If the `v-....png` files does not show the convergence of velocity components to steady state, larger `endTime` should be set in `system/controlDict`, and simulations should be run again.

**Vizualization of obtained results**

When simulation is completed, the results will be automatically vizualized. In the current directory, `compare-...png` files will be created, and in the simulation `VTK` directories, fields from the latest saved time step will be written. To open the files from `VTK` directories, *ParaView* program is necessary.



# Using custom .stl file for obstacle geometry

To use a custom, user-provided `.stl` file, the following steps should be performed:
1. Start in a new, clean calculation folder, e.g. by cloning the repository again:
```
mkdir new-case
cd new-case
git clone https://gitlab.com/eurocc-latvia/culvert-model.git
cd culvert-model
chmod +x *.sh
```
2. [Prepare the environment](#preparation-of-the-environment), if necessary.
3. Copy your `.stl` file into `4-simulations-with-sHM-obstacle/constant/triSurface/obstacle.stl`. This file can be prepared in any CAD software (*Salome*, *AutoCAD* etc.), however there are some requirements for this file:  
  - it must be in ASCII format (not binary),
  - it must be positioned correctly with respect to culvert
4. Manually change the box coordinates in `3-simulations/system/snappyHexMeshDict` file, lines 41-42, to include the approximate region where obstacle is located
5. Run the mesh preparation script: `./4-prepare-all-custom-stl.sh`. After it is completed, mesh can be viewed in the `4-simulations-with-sHM-obstacle/VTK` folder, and `mesh-*.png` files will appear in the current directory. Please examine these files to check if mesh has been generated correctly. If the mesh is created inside the `.stl` (and not outside it), then return to step **1** and then manually change the `locationInMesh` string in line 154 of `3-simulations/system/snappyHexMeshDict`, which should contain coordinates of an arbitrary point *outside* the obstacle, but inside the culvert.
5. [Run the simulations](#running-water-flow-simulations).
6. If it does not converge, try to decrease `relaxationFactors` in `system/fvSolution`. If it does not help, return to step **1** (it is important to copy a "clean" case before running the mesh preparation script) and repeat these steps using different mesh parameters in `parameters.py`. 



# Modification of the case

A user can modify geometric parameters of the system, location of the obstacle, and the total mass flux of water (in cubic meters per second) by changing `parameters.py` file. For example, the influence of the mass flux of water is shown in the figure below.

<img src="img_README/compare-XZ-slice-Q.png" width="650" alt="Example of obtained results: water velocity in the longitudinal vertical slice of the culvert">