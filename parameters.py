# GEOMETRY PARAMETERS --------------------------------------------------------
# all geometry parameters are given in meters

culvert_length   = 4.9
culvert_diameter = 1.3
culvert_height   = 0.4

reservoir_length = 5
reservoir_width  = 3
reservoir_height = 2

obstacle_x = 0.51
obstacle_r = 0.21



# WATER FLOW PARAMETERS -----------------------------------------------------
initial_water_level = 0.3 # initial water level in the culvert, m
flow_rate = 0.4           # inlet flow rate of water, m^3/s




# MESH PARAMETERS ---------------------------------------------------------

N_sq_cylinder  = 40 # number of cells in the core of the cylinder
N_out_cylinder = 20 # number of cells in the outer part of the cylinder
n_x_cylinder   = 22 # mesh density (number of cells in one meter) along x axis in the cylinder

n_x_reservoir = 30 # mesh density (number of cells in one meter) along x axis in the reservoir, approximate
n_y_reservoir = 30 # mesh density (number of cells in one meter) along y axis in the reservoir, approximate
n_z_reservoir = 30 # mesh density (number of cells in one meter) along z axis in the reservoir, approximate

n_proc = 16 # number of processors to run parallel calculations
